#--------------------------------------------------------------------------#
#----------------------------INSIDE DOCKER---------------------------------#
#--------------------------------------------------------------------------#

alias l="ls -a"
#Setting Environment variables for PEER0
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/users/Admin@superAdminOrg.slm.com/msp
CORE_PEER_ADDRESS=peer0.superAdminOrg.slm.com:7051
CORE_PEER_LOCALMSPID="superAdminOrgMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt

#set channel name
export CHANNEL_NAME=mychannel

#channel create
peer channel create -o orderer.slm.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem

#channel join
peer channel join -b mychannel.block


#Setting Env for peer0 slmAdminOrg and join the channel
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/users/Admin@slmAdminOrg.slm.com/msp 
CORE_PEER_ADDRESS=peer0.slmAdminOrg.slm.com:9051 
CORE_PEER_LOCALMSPID="slmAdminOrgMSP" 
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt 
peer channel join -b mychannel.block

#update the anchor peer for peer0 slmAdminOrg
peer channel update -o orderer.slm.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/slmAdminOrgMSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem


#update the anchor peer for peer0 slmUserOrg
CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmUserOrg.slm.com/users/Admin@slmUserOrg.slm.com/msp 
CORE_PEER_ADDRESS=peer0.slmUserOrg.slm.com:11051 
CORE_PEER_LOCALMSPID="slmUserOrgMSP" 
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmUserOrg.slm.com/peers/peer0.slmUserOrg.slm.com/tls/ca.crt 
peer channel join -b mychannel.block

peer channel update -o orderer.slm.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/slmUserOrgMSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem
