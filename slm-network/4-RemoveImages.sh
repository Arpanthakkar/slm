#stop/remove docker containers
docker rm -f $(docker ps -aq)
docker image prune
docker volume prune
docker ps -a
docker images