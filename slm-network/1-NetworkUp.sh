#generate certs
#../bin/cryptogen generate --config=./crypto-config.yaml

#export fabric path
export FABRIC_CFG_PATH=$PWD

#generate block
../bin/configtxgen -profile SampleMultiNodeEtcdRaft -channelID slmnetwork-sys-channel -outputBlock ./channel-artifacts/genesis.block

#creating channel artifacts
#export CHANNEL_NAME=mychannel  && ../bin/configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
export CHANNEL_NAME=mychannel  && ../bin/configtxgen -profile TwoOrgsChannel -outputCreateChannelTx ./channel-artifacts/channel.tx -channelID $CHANNEL_NAME
#org 1 MSP
../bin/configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/superAdminOrgMSPanchors.tx -channelID $CHANNEL_NAME -asOrg superAdminOrgMSP

#org 2 MSP
../bin/configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/slmAdminOrgMSPanchors.tx -channelID $CHANNEL_NAME -asOrg slmAdminOrgMSP

../bin/configtxgen -profile TwoOrgsChannel -outputAnchorPeersUpdate ./channel-artifacts/slmUserOrgMSPanchors.tx -channelID $CHANNEL_NAME -asOrg slmUserOrgMSP

#setting ca env
export SLMNETWORK_CA1_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/superAdminOrg.slm.com/ca && ls *_sk)
export SLMNETWORK_CA2_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/slmAdminOrg.slm.com/ca && ls *_sk)
export SLMNETWORK_CA3_PRIVATE_KEY=$(cd crypto-config/peerOrganizations/slmUserOrg.slm.com/ca && ls *_sk)


#up the docker containers
docker-compose -f docker-compose-cli.yaml -f docker-compose-couch.yaml -f docker-compose-etcdraft2.yaml -f docker-compose-ca.yaml -f docker-compose-couch-wallet.yaml up -d

#check containers
docker ps -a

#into cli
docker exec -it cli bash