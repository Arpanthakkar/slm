# before packaging Go chaincode, vendoring Go dependencies is required like the following commands.
cd /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/document/go
GO111MODULE=on go mod vendor
cd -

# this packages a Go chaincode.
# make note of the --lang flag to indicate "golang" chaincode
# for Go chaincode --path takes the relative path from $GOPATH/src
# The --label flag is used to create the package label

export SEQUENCE=1
export CHANNEL_NAME=mychannel
export LABEL=mydocument

peer lifecycle chaincode package mydocument.tar.gz --path github.com/hyperledger/fabric-samples/chaincode/document/go/ --lang golang --label $LABEL

# this command installs a chaincode package on your peer
peer lifecycle chaincode install mydocument.tar.gz

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name mydocument --version 1.0 --collections-config /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/document/collections_config.json --package-id $CC_PACKAGE_ID --sequence $SEQUENCE --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem

# Save the package ID as an environment variable.REMEMBER DIFFERENT EVERYTIME

peer lifecycle chaincode queryinstalled >&log.txt
res=$?
set +x
cat log.txt
CC_PACKAGE_ID=`sed -n '/Package/{s/^Package ID: //; s/, Label:.*$//; p;}' log.txt`


CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/users/Admin@superAdminOrg.slm.com/msp
CORE_PEER_ADDRESS=peer0.superAdminOrg.slm.com:7051
CORE_PEER_LOCALMSPID="superAdminOrgMSP"
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt

peer lifecycle chaincode install mydocument.tar.gz

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name mydocument --version 1.0 --collections-config /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/document/collections_config.json --package-id $CC_PACKAGE_ID --sequence $SEQUENCE --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem

CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/users/Admin@slmAdminOrg.slm.com/msp 
CORE_PEER_ADDRESS=peer0.slmAdminOrg.slm.com:9051 
CORE_PEER_LOCALMSPID="slmAdminOrgMSP" 
CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt 

peer lifecycle chaincode install mydocument.tar.gz

peer lifecycle chaincode approveformyorg --channelID $CHANNEL_NAME --name mydocument --version 1.0 --collections-config /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/document/collections_config.json --package-id $CC_PACKAGE_ID --sequence $SEQUENCE --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem

# this approves a chaincode definition for your org
# make note of the --package-id flag that provides the package ID
# use the flag to request the ``Init`` function be invoked to initialize the chaincode

# except for --package-id which is not required since it is not stored as part of
# the definition
peer lifecycle chaincode checkcommitreadiness --channelID $CHANNEL_NAME --name mydocument --version 1.0 --sequence $SEQUENCE --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem --output json

# this commits the chaincode definition to the channel
peer lifecycle chaincode commit -o orderer.slm.com:7050  --channelID $CHANNEL_NAME --name mydocument --version 1.0 --sequence $SEQUENCE --collections-config /opt/gopath/src/github.com/hyperledger/fabric-samples/chaincode/document/collections_config.json --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem  --peerAddresses peer0.superAdminOrg.slm.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt  --peerAddresses peer0.slmAdminOrg.slm.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt --peerAddresses peer0.slmUserOrg.slm.com:11051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmUserOrg.slm.com/peers/peer0.slmUserOrg.slm.com/tls/ca.crt 

# ----------------------------------------- Public database -------------------------------------------------------------
#invoke init Ledger function
peer chaincode invoke -o orderer.slm.com:7050 --isInit --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem -C $CHANNEL_NAME -n mydocument --peerAddresses peer0.superAdminOrg.slm.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt --peerAddresses peer0.slmAdminOrg.slm.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt -c '{"Args":["InitLedger"]}' --waitForEvent

#Invoke to create a document
peer chaincode invoke -o orderer.slm.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem -C $CHANNEL_NAME -n mydocument --peerAddresses peer0.superAdminOrg.slm.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt --peerAddresses peer0.slmAdminOrg.slm.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt -c '{"Args":["CreateDocument","DOCUMENT2","ewogIm1ldGFkYXQxIjoibmFtZSIsCiAibWV0YWRhdDIiOiJnb3Zlcm5tZW50IiwKICJtZXRhZGF0MyI6MTI1LAogInRlbXBsYXRlMSI6Im5hbWUiLAogInRlbXBsYXRlMiI6Im5hbWUiCn0=","false"]}' --waitForEvent 

#query the document from the ledger
peer chaincode query -n mydocument -C $CHANNEL_NAME -c '{"Args":["QueryDocument","DOCUMENT2"]}'

#Update access control for a document
peer chaincode invoke -o orderer.slm.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem -C $CHANNEL_NAME -n mydocument --peerAddresses peer0.superAdminOrg.slm.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt --peerAddresses peer0.slmAdminOrg.slm.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt -C $CHANNEL_NAME -c '{"Args":["AddDocumentAccess","DOCUMENT1","[\"c3\",\"c4\"]"]}' --waitForEvent 

#Query all Documents
peer chaincode query -n mydocument -C $CHANNEL_NAME -c '{"Args":["QueryAllDocuments"]}'

# -------------------------------------- Private Database -----------------------------------------------------------------
export DOCUMENT=$(echo -n "{\"DocumentNumber\" : \"wd23e\",\"DocumentObject\" : \"asdvew\",\"Thumbnails\" : \"poasdjv0923yfr92hfioadncvo023fjow\",\"Public\" : \"true\"}" | base64 | tr -d \\n)
#invoke init Ledger function
peer chaincode invoke -o orderer.slm.com:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/slm.com/orderers/orderer.slm.com/msp/tlscacerts/tlsca.slm.com-cert.pem -C $CHANNEL_NAME -n mydocument --peerAddresses peer0.superAdminOrg.slm.com:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/superAdminOrg.slm.com/peers/peer0.superAdminOrg.slm.com/tls/ca.crt --peerAddresses peer0.slmAdminOrg.slm.com:9051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/slmAdminOrg.slm.com/peers/peer0.slmAdminOrg.slm.com/tls/ca.crt -c '{"Args":["CreateDocument","[]"]}' --transient "{\"document\":\"$DOCUMENT\"}" --waitForEvent 

#query the document from the ledger
peer chaincode query -n mydocument -C $CHANNEL_NAME -c '{"Args":["QueryDocument","wd23e"]}'

eyJEb2N1bWVudE51bWJlciIgOiAid2QyM2UiLCJEb2N1bWVudE9iamVjdCIgOiAiYXNkdmV3IiwiVGh1bWJuYWlscyIgOiAicG9hc2RqdjA5MjN5ZnI5MmhmaW9hZG5jdm8wMjNmam93IiwiUHVibGljIiA6ICJ0cnVlIn0=

