/* eslint-disable security/detect-non-literal-fs-filename */
/* eslint-disable no-console */
const { Gateway, Wallets } = require('fabric-network');
const path = require('path');
const fs = require('fs');
const logger = require('../config/logger');
const { CouchDBWalletConfig } = require('../config/couchConfig');

module.exports.queryDetails = async (org, username, channelName, contractName, functionName, uniqueId) => {
  try {
    // load the network configuration
    const ccpPath = path.resolve(__dirname, 'connection', `connection-${org}.json`);
    const ccp = JSON.parse(fs.readFileSync(ccpPath, 'utf8'));

    // Create a new file system based wallet for managing identities.
    const wallet = await Wallets.newCouchDBWallet(CouchDBWalletConfig.url, org.toLowerCase());

    // Check to see if we've already enrolled the user.
    const identity = await wallet.get(username);
    if (!identity) {
      logger.info('An identity for the user "appUser" does not exist in the wallet');
      logger.info('Run the registerUser.js application before retrying');
      throw `An identity for the user: ${username}, does not exist in the wallet`;
    }

    // Create a new gateway for connecting to our peer node.
    const gateway = new Gateway();
    await gateway.connect(ccp, { wallet, identity: username, discovery: { enabled: true, asLocalhost: true } });

    // Get the network (channel) our contract is deployed to.
    const network = await gateway.getNetwork(channelName);

    // Get the contract from the network.
    const contract = network.getContract(contractName);

    // Evaluate the specified transaction.
    const result = await contract.evaluateTransaction(functionName, uniqueId);
    // logger.info(`Transaction has been evaluated, result is: ${result.toString()}`);
    return result.toString();
  } catch (error) {
    console.error(`Failed to evaluate transaction: ${error}`);
    throw error;
  }
};
