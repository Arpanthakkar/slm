module.exports = {
  db: {
    str: 'mongodb://127.0.0.1:27017/doctrace',
    // str: process.env.MONGO_URL,
    options: {
      auto_reconnect: true,
      reconnectTries: Number.MAX_SAFE_INTEGER,
      poolSize: 200,
      useNewUrlParser: true,
      readPreference: 'primaryPreferred',
    },
  },
};
