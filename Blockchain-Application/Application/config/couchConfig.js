module.exports = {
  DocumentChaincode: {
    channelName: 'mychannel',
    contractName: 'mydocument',
    functionNames: {
      createDocument: 'CreateDocument',
      queryDocument: 'QueryDocument',
      queryAllDocuments: 'QueryAllDocuments',
      addDocumentAccess: 'AddDocumentAccess',
    },
  },
  CouchDBWalletConfig: {
    url: process.env.DOCTRACE_COUCHDB_WALLET_URL,
    db: 'document',
  },
};
