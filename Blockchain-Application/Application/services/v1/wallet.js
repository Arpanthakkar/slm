const enrollAdmin = require('../../sdk/enrollAdmin');
const registerUser = require('../../sdk/registerUser');
const logger = require('../../config/logger');


module.exports = {
  // Enroll Admin needs to be called only once
  addAdmin: async (org) => {
    logger.info('Inside Enroll Admin Service');
    try {
      const enroll = await enrollAdmin.enrollAdmin(org);
      return enroll;
    } catch (error) {
      logger.error('error', error);
      throw error;
    }
  },
  createUserWallet: async (org, username) => {
    logger.info('Inside Register User Service');
    try {
      const reg = await registerUser.registerUser(org, username);
      return reg;
    } catch (error) {
      logger.error('error', error);
      throw error;
    }
  },
};
