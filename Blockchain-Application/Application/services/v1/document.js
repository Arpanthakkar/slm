const Document = require('../../models/documentModel');
const invokeChaincode = require('../../sdk/invoke');
const queryChaincode = require('../../sdk/query');
const fs  = require('fs');
var path = require('path');


const { ObjectId } = require('mongoose').Types
const {channelName, contractName, functionNames} =  require('../../config/couchConfig').DocumentChaincode


module.exports = {
    
    addOffChainData: async (fields) => {
        try {
            const newDocument = await Document.create(fields);
            return newDocument;
        }
        catch (err) {
            return err;
        }
    },
    addOnChainData: async (org, username, arguments) => {
        try {
            const newDocument = await invokeChaincode.invokeChaincode(org, username, channelName, contractName, functionNames.createDocument, arguments);
            return newDocument;
        }
        catch (err) {
            return err;
        }
    },

    queryDocument: async (org, username, documentId)=>{
        try {
            let query = await queryChaincode.queryDetails(org, username, channelName, contractName, functionNames.queryDocument, documentId);
            return JSON.parse(query);
        } 
        catch (err) {
            return err;
        }        
    },

    getOffChainData: async (username, documentId)=>{
        try {
            let query = await Document.findOne({"_id": documentId});
            return query;
        } 
        catch (err) {
            return err;
        }        
    },

    addAccessControl: async (org, username, arguments)=>{
        try {
            const addAccess = await invokeChaincode.invokeChaincode(org, username, channelName, contractName, functionNames.addDocumentAccess, arguments)
            return addAccess;
        } 
        catch (err) {
            return err;
        }        
    },

    updateDocumentObject: async (req, obj)=>{
        try {
            const addAccess = await invokeChaincode.invokeChaincode(org, username, channelName, contractName, functionNames.addDocumentAccess, arguments)
            return addAccess;
        } 
        catch (err) {
            return err;
        }        
    },

    base64Encode: async (file) => {
        // convert binary data to base64 encoded string
        return new Buffer(file).toString('base64');
    },

    base64Decode: async (base64str, filename) => {

        const buf = Buffer.from(base64str, 'base64');
        const imagePath = path.join('./uploads/',filename)

        fs.writeFile(imagePath, buf, (error)=> {
          if(error){
            throw error;
          }else{
            return JSON.stringify(imagePath);
          }
        });
    },

    getPath: async (str2) => {
        const str1 = './';
        str2 = str2.replace(/^"(.*)"$/, '$1');
        const path = str1.concat(str2);
        
        return path;
    },

    combineData: async (data, offChainData) => {

        //const image = await this.base64Decode(data.thumbnails, 'image12123');
        const onChainData = await JSON.parse(Buffer(data.documentObject, 'base64').toString('ascii'));
        const accessControl = data.accessControl;
        const public = data.public;

        const obj = {accessControl, public, onChainData, offChainData};

        return obj;
    },
    
    filterDocuments:  async (filterBy, filterData) => {
        try {
            const query = {};
            query [filterBy] = filterData;
            let data = await Document.find(query);
            return data;
        } 
        catch (err) {
          throw err;
        }
      },
}
