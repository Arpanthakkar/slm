package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type SmartContract struct {
	contractapi.Contract
}

//user structure
type UserDetails struct {
	Id             string              `json:"userid"`
	LoanData       []LoanDetails       `json:"loanData"`
	BorrowerData   []BorrowerDetails   `json:"borrowerData"`
	CoBorrowerData []CoBorrowerDetails `json:"coBorrowerData"`
	PropertyData   []PropertyDetails   `json:"propertyData"`
}

//loan structure
type LoanDetails struct {
	Amount       string `json:"amount"`
	Purpose      string `json:"purpose"`
	Occupancy    string `json:"occupancy"`
	PropertyType string `json:"propertyType"`
	EmpType      string `json:"empType"`
	LoanProgram  string `json:"loanProgram"`
}

//borrowerStructure
type BorrowerDetails struct {
	PersonalData    []PersonalDetails   `json:"personalData"`
	EmploymentData  []EmploymentDetails `json:"employmentData"`
	DeclarationData []bool              `json:"declarationData"`
}

//co borrower structure
type CoBorrowerDetails struct {
	PersonalData    []PersonalDetails   `json:"personalData"`
	EmploymentData  []EmploymentDetails `json:"employmentData"`
	DeclarationData []bool              `json:"declarationData"`
}

//personal details struct
type PersonalDetails struct {
	FirstName     string `json:"firstName"`
	MiddleName    string `json:"middleName"`
	LastName      string `json:"lastName"`
	Dob           string `json:"dob"`
	Email         string `json:"email"`
	Phone         string `json:"phoneNumber"`
	EscrowAccount string `json:"escrowAccount"`
	PinCode       string `json:"pincode"`
	CreditScore   string `json:"creditScore"`
}

//Employment details
type EmploymentDetails struct {
	BusinessName string `json:"businessName"`
	BusinessType string `json:"businessType"`
	TurnOver     string `json:"turnOver"`
}

type PropertyDetails struct {
	Location         string `json:"location"`
	PropertyValue    string `json:"propertyValue"`
	Zip              string `json:"zip"`
	BuildYear        string `json:"buildYear"`
	OriginalCost     string `json:"originalCost"`
	ExistingLoan     string `json:"existingLoanAmount"`
	PresentLotValue  string `json:"presentLotValue"`
	CostImprovements string `json:"costImprovements"`
	Total            string `json:"total"`
}

type QueryResult struct {
	Record *UserDetails
}

func (s *SmartContract) AddUserDetails(ctx contractapi.TransactionContextInterface, id string, amount string, purpose string, occupancy string, propertyType string, employmentType string, loanProgram string, borrowerInfo []byte, borroweremp []byte, coborrowerinfo []byte, coborroweremp []byte, location string, propertyValue string, zip string, buildYear string, originalCost string, existingLoanAmount string, presentLotValue string, costImprovements string, total string, borrowerDeclarations []bool, coBorrowerDeclaration []bool) error {

	dataAsBytes, err := ctx.GetStub().GetState(id)
	if err != nil {
		return errors.New("Failed to get transaction")
	} else if dataAsBytes != nil {
		fmt.Println("User id already exist " + id)
		return errors.New("User id already exists: " + id)
	} else {
		loan := []LoanDetails{}
		loanData := LoanDetails{
			Amount:       amount,
			Purpose:      purpose,
			Occupancy:    occupancy,
			PropertyType: propertyType,
			EmpType:      employmentType,
			LoanProgram:  loanProgram,
		}
		loan = append(loan, loanData)

		var borrowerinfo []PersonalDetails
		json.Unmarshal(borrowerInfo, &borrowerinfo)
		borrowerPersonal := []PersonalDetails{}

		for i := range borrowerinfo {

			personalBorrower := PersonalDetails{
				FirstName:     borrowerinfo[i].FirstName,
				MiddleName:    borrowerinfo[i].MiddleName,
				LastName:      borrowerinfo[i].LastName,
				Dob:           borrowerinfo[i].Dob,
				Email:         borrowerinfo[i].Email,
				Phone:         borrowerinfo[i].Phone,
				EscrowAccount: borrowerinfo[i].EscrowAccount,
				PinCode:       borrowerinfo[i].PinCode,
				CreditScore:   borrowerinfo[i].CreditScore,
			}
			borrowerPersonal = append(borrowerPersonal, personalBorrower)

		}

		var borrowerEmpInfo []EmploymentDetails
		json.Unmarshal(borroweremp, &borrowerEmpInfo)
		borrowerEmployment := []EmploymentDetails{}
		for i := range borrowerEmpInfo {

			employmentBorrower := EmploymentDetails{
				BusinessName: borrowerEmpInfo[i].BusinessName,
				BusinessType: borrowerEmpInfo[i].BusinessType,
				TurnOver:     borrowerEmpInfo[i].TurnOver,
			}
			borrowerEmployment = append(borrowerEmployment, employmentBorrower)
		}

		borrower := []BorrowerDetails{}
		borrowerData := BorrowerDetails{
			PersonalData:    borrowerPersonal,
			EmploymentData:  borrowerEmployment,
			DeclarationData: borrowerDeclarations,
		}
		borrower = append(borrower, borrowerData)

		var coborrowerInfo []PersonalDetails
		json.Unmarshal(coborrowerinfo, &coborrowerInfo)
		coBorrowerPersonal := []PersonalDetails{}

		for i := range coborrowerInfo {

			personalCoBorrower := PersonalDetails{
				FirstName:     coborrowerInfo[i].FirstName,
				MiddleName:    coborrowerInfo[i].MiddleName,
				LastName:      coborrowerInfo[i].LastName,
				Dob:           coborrowerInfo[i].Dob,
				Email:         coborrowerInfo[i].Email,
				Phone:         coborrowerInfo[i].Phone,
				EscrowAccount: coborrowerInfo[i].EscrowAccount,
				PinCode:       coborrowerInfo[i].PinCode,
				CreditScore:   coborrowerInfo[i].CreditScore,
			}
			coBorrowerPersonal = append(coBorrowerPersonal, personalCoBorrower)
		}

		var coborrowerEmpInfo []EmploymentDetails
		json.Unmarshal(coborroweremp, &coborrowerEmpInfo)
		coBorrowerEmployment := []EmploymentDetails{}

		for i := range coborrowerEmpInfo {

			employmentCoBorrower := EmploymentDetails{
				BusinessName: coborrowerEmpInfo[i].BusinessName,
				BusinessType: coborrowerEmpInfo[i].BusinessType,
				TurnOver:     coborrowerEmpInfo[i].TurnOver,
			}
			coBorrowerEmployment = append(coBorrowerEmployment, employmentCoBorrower)
		}

		coborrower := []CoBorrowerDetails{}
		coBorrowerData := CoBorrowerDetails{
			PersonalData:    coBorrowerPersonal,
			EmploymentData:  coBorrowerEmployment,
			DeclarationData: coBorrowerDeclaration,
		}
		coborrower = append(coborrower, coBorrowerData)

		property := []PropertyDetails{}
		propertyData := PropertyDetails{
			Location:         location,
			PropertyValue:    propertyValue,
			Zip:              zip,
			BuildYear:        buildYear,
			OriginalCost:     originalCost,
			ExistingLoan:     existingLoanAmount,
			PresentLotValue:  presentLotValue,
			CostImprovements: costImprovements,
			Total:            total,
		}
		property = append(property, propertyData)

		details := UserDetails{
			Id:             id,
			LoanData:       loan,
			BorrowerData:   borrower,
			CoBorrowerData: coborrower,
			PropertyData:   property,
		}
		txAsBytes, _ := json.Marshal(details)

		return ctx.GetStub().PutState(id, txAsBytes)
	}
}

func (s *SmartContract) QuerySingleUserData(ctx contractapi.TransactionContextInterface, id string) (*UserDetails, error) {
	UserAsBytes, err := ctx.GetStub().GetState(id)

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if UserAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", id)
	}

	data := new(UserDetails)
	_ = json.Unmarshal(UserAsBytes, data)

	return data, nil
}

func (s *SmartContract) QueryAllUserData(ctx contractapi.TransactionContextInterface, richQuery string) ([]QueryResult, error) {
	//
	queryString := fmt.Sprintf(richQuery)
	resultIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, errors.New("Enter valid Query")
	}
	defer resultIterator.Close()
	//
	results := []QueryResult{}
	//
	for resultIterator.HasNext() {
		queryResponse, err := resultIterator.Next()
		//
		if err != nil {
			return nil, errors.New("Enter valid details")
		}

		data := new(UserDetails)
		_ = json.Unmarshal(queryResponse.Value, data)

		queryResult := QueryResult{Record: data}
		results = append(results, queryResult)
	}
	return results, nil
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error create userdetail chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting userdetail chaincode: %s", err.Error())
	}
}
