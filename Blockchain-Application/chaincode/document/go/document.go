package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// SmartContract provides functions for managing a document
type SmartContract struct {
	contractapi.Contract
}

// type User struct {
// 	CommonName string `json:"commonName"`
// }

// Document describes basic details of what DocumentNumbers up a document
type Document struct {
	ObjectType     string   `json:"objectType"`
	DocumentObject string   `json:"documentObject"`
	Thumbnails     string   `json:"thumbnails"`
	AccessControl  []string `json:"accessControl"`
	Public         string   `json:"public"`
}

// QueryResult structure used for handling result of query
type QueryResult struct {
	Key    string `json:"Key"`
	Record *Document
}

// InitLedger adds a base set of Documents to the ledger
// func (s *SmartContract) InitLedger(ctx contractapi.TransactionContextInterface) (string, error) {
// 	documents := []Document{
// 		Document{DocumentObject: "35635745745673562454trdhfhnweyetykahzdfh", Thumbnails: "joesfnoiawenevoksdndvloierw3463", AccessControl: []string{"c1", "c2"}, Public: "false"},
// 	}

// 	for i, document := range documents {
// 		documentAsBytes, _ := json.Marshal(document)
// 		err := ctx.GetStub().PutState("DOCUMENT"+strconv.Itoa(i), documentAsBytes)

// 		if err != nil {
// 			return "", fmt.Errorf("Failed to put to world state. %s", err.Error())
// 		}
// 	}

// 	return "Init Ledger was successful", nil
// }

// CreateDocument adds a new document to the world state with given details
func (s *SmartContract) CreateDocument(ctx contractapi.TransactionContextInterface, args []string) (string, error) {

	type documentTransientInput struct {
		DocumentNumber string   `json:"documentNumber"`
		DocumentObject string   `json:"documentObject"`
		Thumbnails     string   `json:"thumbnails"`
		AccessControl  []string `json:"accessControl"`
		Public         string   `json:"public"`
	}

	if len(args) != 0 {
		return "", fmt.Errorf("Incorrect number of arguments. Private marble data must be passed in transient map")
	}

	transMap, err := ctx.GetStub().GetTransient()
	if err != nil {
		return "", fmt.Errorf("Error getting transient: %s", err)
	}

	documentJSONBytes, ok := transMap["document"]
	if !ok {
		return "", fmt.Errorf("document must be a key in the transient map")
	}

	if len(documentJSONBytes) == 0 {
		return "", fmt.Errorf("document value in the transient map must be a non-empty JSON string")
	}

	var documentInput documentTransientInput
	err = json.Unmarshal(documentJSONBytes, &documentInput)
	if err != nil {
		return "", fmt.Errorf("Failed to decode JSON of: %s", err)
	}

	clientid := ctx.GetClientIdentity()
	x509, _ := clientid.GetX509Certificate()
	commonName := x509.Subject.CommonName

	var accessControl []string

	x := append(accessControl, commonName)

	if len(documentInput.DocumentObject) == 0 {
		return "", fmt.Errorf("Document object with metadata and attribute cannot be null")
	}
	if len(documentInput.Thumbnails) == 0 {
		return "", fmt.Errorf("Thumbnails cannot be null")
	}
	if len(x) == 0 {
		return "", fmt.Errorf("You need to provide access control to atleast one user")
	}

	// ==== Check if document already exists ====
	documentAsBytes, err := ctx.GetStub().GetPrivateData("collectionDocuments", documentInput.DocumentObject)
	if err != nil {
		return "", fmt.Errorf("Failed to get document: %s", err)
	} else if documentAsBytes != nil {
		fmt.Println("This document already exists: " + documentInput.DocumentObject)
		return "", fmt.Errorf("This document already exists: %s", documentInput.DocumentObject)
	}

	document := Document{
		ObjectType:     "document",
		DocumentObject: documentInput.DocumentObject,
		Thumbnails:     documentInput.Thumbnails,
		AccessControl:  x,
		Public:         documentInput.Public,
	}

	documentJSONAsBytes, _ := json.Marshal(document)
	if err != nil {
		return "", fmt.Errorf("Error during marshal")
	}

	err = ctx.GetStub().PutPrivateData("collectionDocuments", documentInput.DocumentNumber, documentJSONAsBytes)

	if err != nil {
		return "", fmt.Errorf("Put Document failed %s", err)
	}
	//return ctx.GetStub().PutState(documentNumber, documentAsBytes)
	return "Document submitted succesfully", nil
}

// QueryDocument returns the document stored in the world state with given id
func (s *SmartContract) QueryDocument(ctx contractapi.TransactionContextInterface, documentNumber string) (*Document, error) {
	var count int
	documentAsBytes, err := ctx.GetStub().GetPrivateData("collectionDocuments", documentNumber)

	clientid := ctx.GetClientIdentity()
	x509, _ := clientid.GetX509Certificate()
	commonName := x509.Subject.CommonName

	if err != nil {
		return nil, fmt.Errorf("Failed to read from world state. %s", err.Error())
	}

	if documentAsBytes == nil {
		return nil, fmt.Errorf("%s does not exist", documentNumber)
	}

	document := new(Document)
	_ = json.Unmarshal(documentAsBytes, document)

	for _, user := range document.AccessControl {
		if commonName == user {
			count = 1
		}
	}
	if count != 1 {
		return nil, fmt.Errorf("you do not have access to document %s", documentNumber)
	}

	return document, nil
}

// QueryAllDocuments returns all documents found in world state
// func (s *SmartContract) QueryAllDocuments(ctx contractapi.TransactionContextInterface) ([]QueryResult, error) {
// 	startKey := "DOCUMENT0"
// 	endKey := "DOCUMENT99"

// 	resultsIterator, err := ctx.GetStub().GetStateByRange(startKey, endKey)

// 	if err != nil {
// 		return nil, err
// 	}
// 	defer resultsIterator.Close()

// 	results := []QueryResult{}

// 	for resultsIterator.HasNext() {
// 		queryResponse, err := resultsIterator.Next()

// 		if err != nil {
// 			return nil, err
// 		}

// 		document := new(Document)
// 		_ = json.Unmarshal(queryResponse.Value, document)

// 		queryResult := QueryResult{Key: queryResponse.Key, Record: document}
// 		results = append(results, queryResult)
// 	}

// 	return results, nil
// }

// QueryAllDocuments returns all documents found in world state
func (s *SmartContract) QueryAllDocuments(ctx contractapi.TransactionContextInterface) ([]QueryResult, error) {
	queryString := fmt.Sprintf(`{
		"selector": {
		   "objectType": "document"
		}
	 }`)
	resultIterator, err := ctx.GetStub().GetQueryResult(queryString)
	if err != nil {
		return nil, errors.New("Enter valid details")
	}
	defer resultIterator.Close()
	//
	results := []QueryResult{}
	//
	for resultIterator.HasNext() {
		queryResponse, err := resultIterator.Next()
		//
		if err != nil {
			return nil, errors.New("Enter valid details")
		}

		d := new(Document)
		_ = json.Unmarshal(queryResponse.Value, d)

		queryResult := QueryResult{Record: d}
		results = append(results, queryResult)
	}
	fmt.Println(len(results))
	return results, nil
}

// AddDocumentAccess updates the owner field of document with given id in world state
func (s *SmartContract) AddDocumentAccess(ctx contractapi.TransactionContextInterface, documentNumber string, newUser []string) error {
	document, err := s.QueryDocument(ctx, documentNumber)

	if err != nil {
		return err
	}
	for _, user := range newUser {
		document.AccessControl = append(document.AccessControl, user)
	}

	documentAsBytes, _ := json.Marshal(document)

	return ctx.GetStub().PutState(documentNumber, documentAsBytes)
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(SmartContract))

	if err != nil {
		fmt.Printf("Error create fabdocument chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting fabdocument chaincode: %s", err.Error())
	}
}

// {
// 	"selector": {
// 	   "documentObject": "doc"
// 	}
// }
